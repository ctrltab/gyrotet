self.addEventListener("install", function (e) {
  e.waitUntil(
    caches.open("gyrotet").then(function (cache) {
      return cache.addAll([
        "/",
        "/index.html",
        "/pixi.js",
        "/pixi.js.map",
        "/normalize.css",
      ]);
    })
  );
});

self.addEventListener("fetch", function (event) {
console.log('Handling fetch event for', event.request.url);
  event.respondWith(
    caches.open("gyrotet").then(function (cache) {
      return cache
        .match(event.request)
        .then(function (response) {
          if (response) {
          console.log(' Found response in cache:', response);
            return response;
          }
          
          console.log(' No response for %s found in cache. About to fetch ' +
          'from network...', event.request.url);

          return fetch(event.request.clone()).then(function (response) {
            return response;
          });
        })
        .catch(function (error) {
          throw error;
        });
    })
  );
});
